
# === CAPTURE VARS ===

CWD=`pwd`



# === FUNCTIONS ===

should_write_to_location () {  
  if [ -e $1 ]; then
    echo "$1 exists.  Overwrite it? (Y/N)";
    read OVERWRITE;

    while [ $OVERWRITE != "Y" -a $OVERWRITE != "y" -a $OVERWRITE != "N" -a $OVERWRITE != "n" ]; do
      read OVERWRITE;
    done;

    if [ $OVERWRITE == "Y" -o $OVERWRITE == "y" ]; then
      echo "Removing $1";
      rm -rf $1;
      SHOULD_WRITE=1;
    else
      SHOULD_WRITE=0;
    fi;
  else
    SHOULD_WRITE=1;
  fi;
}

# Creates a symlink after asking the user's permission
symlink_if_requested () {
  should_write_to_location $2
  if [ $SHOULD_WRITE -eq 1 ]; then
    echo "Symlinking $2 to $1"
    ln -s $1 $2
  fi
}


# === VIM ====

echo "=== VIM ==="
symlink_if_requested "$CWD/.vimrc" "$HOME/.vimrc"
symlink_if_requested "$CWD/.vim/colors" "$HOME/.vim/colors"
symlink_if_requested "$CWD/.vim/scripts" "$HOME/.vim/scripts"
echo "Installing Vundle plugins"
vim +PluginInstall +qall


# === ZSH ===

echo ""
echo "=== ZSH ==="

symlink_if_requested "$CWD/.zshrc" "$HOME/.zshrc"


# === SHELL SCRIPTS ===

symlink_if_requested "$CWD/.shell-scripts" "$HOME/.shell-scripts"
