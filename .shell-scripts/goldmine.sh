
# copy music to goldmine
copymusic () {

  # ASCIIIIIIIi
  echo "   ___|)___________________________________________________________"
  echo "  |___/____________________________________________________________"
  echo "  |__/|____________________________________________________________"
  echo "  |_/(|,\__________________________________________________________"
  echo "  |_\_|_/__________________________________________________________"
  echo "  |   |  "
  echo "  | (_|   "  
  echo "  |"
  echo "  |________________________________________________________________"
  echo "  |__/___\_._______________________________________________________"
  echo "  |__\___|_._______________________________________________________"
  echo "  |_____/__________________________________________________________"
  echo "  |____/___________________________________________________________"
  echo ""

  # check that params were supplied
  if [[ ( -z "$1" ) || ( -z "$2" ) || ( -z "$3" ) ]]
  then
    echo "Usage: copymusic \"\$(pwd)\" \"BAND\ NAME\" \"ALBUM\ NAME\""
    return 1
  fi

  # print vars
  echo "Copying from $2's album $3 from $1 to the GOLDMINE"

  # compress, copy to goldmine, extract
  tar -C "$1" -cvf /tmp/music.tar.gz .
  scp -r /tmp/music.tar.gz goldmine:~/Music
  rm /tmp/music.tar.gz
  ssh goldmine mkdir Music/tmp
  ssh goldmine tar -xvf Music/music.tar.gz -C Music/tmp

  # move to plex folder
  if ! (ssh goldmine "[ -d /var/lib/plex/Music/$2 ]")
  then
    ssh goldmine mkdir "/var/lib/plex/Music/$2"
  fi
  ssh goldmine mv Music/tmp "/var/lib/plex/Music/$2/$3"

  # cleanup
  ssh goldmine rm -rf Music/tmp Music/music.tar.gz
}
