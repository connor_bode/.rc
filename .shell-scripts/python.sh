
# startup virtualenv-burrito
if [ -f $HOME/.venvburrito/startup.sh ]; then
    . $HOME/.venvburrito/startup.sh
fi

# autoenv config
source /usr/local/opt/autoenv/activate.sh