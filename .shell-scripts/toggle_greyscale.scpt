tell application "System Preferences"
    activate
    reveal anchor "Seeing_Display" of pane id "com.apple.preference.universalaccess"
    tell application "System Events" to tell process "System Preferences"
        click the checkbox "Use grayscale" of window "Accessibility"
    end tell
end tell
tell application "System Preferences" to quit
