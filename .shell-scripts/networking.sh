# change the laptop mac address to a random address
spoof_mac () {
  current=`ifconfig en0 | grep ether`
  old="old MAC address: $current"
  spoof=`openssl rand -hex 6 | sed 's/\(..\)/\1:/g; s/.$//'`
  echo "spoofing MAC as $spoof"
  sudo ifconfig en0 ether $spoof
  echo "$old"
  new=`ifconfig en0 | grep ether`
  echo "new MAC address: $new"
}