echo " php loaded "
echo " -=------=- " 
echo "CMD LIZT: "
echo " - start_php_fpm"
echo " - stop_php_fpm"
echo " - restart_php_fpm" 

start_php_fpm () {
  launchctl load ~/Library/LaunchAgents/homebrew.mxcl.php55.plist
}

stop_php_fpm () {
  launchctl unload ~/Library/LaunchAgents/homebrew.mxcl.php55.plist
}

restart_php_fpm () {
  stop_php_fpm
  sleep 1
  start_php_fpm
}



