# start mongodb
startmongo () {
  echo "running mongo daemon"
  mongod --dbpath ~/.data/
}

echo "========================="
echo " MONGO LOADED"
echo ""
echo " - use 'startmongo' to "
echo "   run teh db "
echo "========================="
