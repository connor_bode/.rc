set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
Plugin 'VundleVim/Vundle.vim'
Plugin 'https://github.com/fatih/vim-go'
Plugin 'ctrlpvim/ctrlp.vim'
Plugin 'https://github.com/scrooloose/nerdtree'
Plugin 'flazz/vim-colorschemes'
Plugin 'https://github.com/severin-lemaignan/vim-minimap'
Plugin 'https://github.com/mustache/vim-mustache-handlebars'
Plugin 'othree/html5.vim'
Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'
call vundle#end()          
filetype plugin indent on    

" Navigation 
set mouse=a
set nu
set wildmenu
set autoindent
set backspace=indent,eol,start

" tabbbs
set expandtab
set tabstop=2
set shiftwidth=2

" Syntax 
syntax on
colorscheme monokai

" Open nerd tree if no file was specified
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif

" map shortcut to nerdtree
map <C-n> :NERDTreeToggle<CR>

" allow arrow usage in nerdtree
let g:NERDTreeDirArrows = 1
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'

" starting directory for ctrlp
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_working_path_mode = 'cr'

" sudo write command
command W w !sudo tee % > /dev/null

" syntax specific
au Filetype python source ~/.vim/scripts/python.vim
